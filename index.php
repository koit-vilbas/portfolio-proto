<?php
    include('classes/Database.class.php');
    include('classes/User.class.php');
    include('settings.php');
    //Dünaamiline veebileht
    include('pages/header.php');
    $_GET['l'] = '';
    $leht = $_GET['l'];

    if(empty($leht)) {
        $leht = 'index';
    }

    if(!file_exists("pages/$leht.php")) {
        $leht = 'index';
    }
    if($leht == 'header' || $leht == 'footer') {
        $leht = 'index';
    }
    $leht = basename($leht);
    include('pages/' . $leht . '.php');

    include('pages/footer.php');
?>