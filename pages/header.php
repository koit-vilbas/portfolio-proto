<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $settings['title']; ?></title>
        <link type="text/css" rel="stylesheet" href="css/main.css">
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <meta charset="utf-8">
    </head>
    <body>
        <div class="header">
            
            <div class="navigation">
                <a href="?l=index"><li>Avaleht</li></a>
                <a href="?l=tood"><li>Tööd</li></a>
                <a href="?l=minust"><li>Minust</li></a>
                <a href="?l=kontakt"><li>Kontakt</li></a>
                <a href="?l=login"><li id="login">Logi sisse</li></a>
            </div>
        </div>
